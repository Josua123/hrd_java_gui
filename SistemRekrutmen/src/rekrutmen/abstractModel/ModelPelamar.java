/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.abstractModel;

import rekrutmen.abstractModel.ModelPelamar;
import rekrutmen.model.Pelamar;

/**
 *
 * @author Sondang;
 */
public class ModelPelamar extends AbstractModel<Pelamar> {
    
    public ModelPelamar() {
        super(Pelamar.class);
    }
    
}

