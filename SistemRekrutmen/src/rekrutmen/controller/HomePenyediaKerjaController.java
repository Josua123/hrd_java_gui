/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import static rekrutmen.controller.HomeController.id;
import static rekrutmen.controller.ValidasiLamaranController.kualifikasiPekerjaans;
import static rekrutmen.controller.ValidasiLamaranController.namaPekerjaans;
import static rekrutmen.controller.ValidasiLamaranController.namaPelamars;
import rekrutmen.model.Lamaran;
import rekrutmen.model.Lowongan;
import rekrutmen.model.Perusahaan;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class HomePenyediaKerjaController implements Initializable {

    @FXML
    private TableView<Lamaran> tablePelamaran;
    @FXML
    private TableColumn rNama_pekerjaan;
    @FXML
    private TableColumn rKualifikasiPekerjaan;
    @FXML
    private TableColumn rNamaPelamar;
    @FXML
    private AnchorPane rootPane;

    ObservableList<Lamaran> listLamaran = null;
    
    ObservableList<Perusahaan> listPerusahaan = null;
    
    private static String QUERY_LAMARAN="FROM Lamaran E WHERE E.idPerusahaan = :id_perusahaan" ;
    
    private static String QUERY_PERUSAHAAN="from Perusahaan" ;
    
    public static int id =0;
    public static String namaPelamars, resumes, namaPerusahaans, namaPekerjaans, kualifikasiPekerjaans, statusLamarans;
    public static int dId;
    
    public static int idLamaran=0;
    @FXML
    private TableColumn rStatusLamaran;
    @FXML
    private TableColumn rResume;
   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tampil();
    }
    
    private void executeHQLQuery(String hql){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("FROM Lamaran E WHERE E.idPerusahaan = :id_perusahaan");
            this.id = LoginPenyediaKerjaController.id;
            q.setParameter("id_perusahaan", id);
            List resultList = q.list();
            displayResult(resultList);
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    private void displayResult(List resultList){
        listLamaran = FXCollections.observableArrayList();
//        PemesananModel pm = new PemesananModel();
        
        for(Object m : resultList){
            Lamaran lamaran = (Lamaran)m;
            listLamaran.add(lamaran);
            idLamaran = lamaran.getIdLamaran();
        }
        
        rNama_pekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobTitle"));
        rKualifikasiPekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobKualification"));
        rNamaPelamar.setCellValueFactory(new PropertyValueFactory<>("namaPelamar"));
        rResume.setCellValueFactory(new PropertyValueFactory<>("resume"));
        rStatusLamaran.setCellValueFactory(new PropertyValueFactory<>("statusLamaran"));
        
        tablePelamaran.getColumns().clear();
        tablePelamaran.setItems(listLamaran);
        tablePelamaran.getColumns().addAll(rNama_pekerjaan, rKualifikasiPekerjaan, rNamaPelamar, rResume, rStatusLamaran);
        
        tablePelamaran.setOnMouseClicked(new EventHandler<MouseEvent>(){
           @Override
           public void handle(MouseEvent event){
               pilihText();
           }
        });
    }
    
    public void pilihText(){
        
        if(tablePelamaran.getSelectionModel().getSelectedItem() != null){
            Lamaran selectedTable = tablePelamaran.getSelectionModel().getSelectedItem();
//            this.ids = selectedTable.getIdLowongan();
//            dId = Integer.toString(this.ids);
//            id.setText(dId);
            this.namaPekerjaans = selectedTable.getJobTitle(); 
            this.namaPelamars = selectedTable.getNamaPelamar();
            this.resumes = selectedTable.getResume();            
            this.kualifikasiPekerjaans = selectedTable.getJobKualification();
            this.statusLamarans = selectedTable.getStatusLamaran();
        }
    }
   
    public void tampil(){
        executeHQLQuery(QUERY_LAMARAN);
    }
    
    @FXML
    private void keluar(MouseEvent event) {
        
        
    }

    @FXML
    private void btnTambahLowongan(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/FormTambahLowongan.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnListLowongan(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/LowonganKami.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnLihatDetail(ActionEvent event) throws IOException {
        if(idLamaran == 0){
            Alert eAlert = new Alert(Alert.AlertType.WARNING);
            eAlert.setHeaderText(null);
            eAlert.setContentText("Tidak ada data yang dipilih");
            eAlert.showAndWait();
        }else{
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/ValidasiLamaran.fxml"));
            rootPane.getChildren().setAll(pane);
        }  
    }

    @FXML
    private void btnLogout(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/LoginPenyediaKerja.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnProfil(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/ProfilPengusaha.fxml"));
        rootPane.getChildren().setAll(pane);
    }
    
}
