/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import org.hibernate.Query;
import org.hibernate.Session;
import rekrutmen.model.Lamaran;
import rekrutmen.model.Pelamar;
import rekrutmen.model.Perusahaan;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class LoginPenyediaKerjaController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TextField email;
    @FXML
    private TextField password;
    
    public static int idPerusahaan=0;
    
    public static String namas, pass, emails, alamat, noTelp, resumes; 

    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    public String savedEmail;
    public String savedPassword;
    private static String QUERY_LOGIN = "from Perusahaan a where a.emailPerusahaan like '";
    
    public static int id = 0;
    
    
    private void runLogin(){
        executeHQLQuery(QUERY_LOGIN + email.getText() + "'");
    }
    
    private void executeHQLQuery(String sql){
        try{
            Session ses = HibernateUtil.getSessionFactory().openSession();
            ses.beginTransaction();
            Query q = ses.createQuery(sql);
            List resultList = q.list();
            for(Object o : resultList){
                Perusahaan p = (Perusahaan)o;
                savedEmail = p.getEmailPerusahaan();
                savedPassword = p.getPassword();
                this.id = p.getIdPerusahaan();
                namas = p.getNamaPerusahaan();
                pass = p.getPassword();
                emails = p.getEmailPerusahaan();
                noTelp = p.getNoTelp();
                alamat = p.getAlamat();
            }
            ses.getTransaction().commit();
            if(savedEmail == null || savedPassword == null){
                autentikasi("", "");
            }else{
                autentikasi(savedEmail, savedPassword);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void autentikasi(String savedEmail, String savedPassword) throws IOException {
        if(savedEmail.equals(email.getText()) && savedPassword.equals(password.getText())){
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/HomePenyediaKerja.fxml"));
            rootPane.getChildren().setAll(pane);
        }else{
            Alert alert = new Alert(Alert.AlertType.NONE, "Silahkan mengisi atau mengecek seluruh data yang ada pada form", ButtonType.OK);
            alert.setTitle("Kesalahan pada Login");
            alert.showAndWait();
        }
    }

    @FXML
    private void btnLogin(ActionEvent event) throws IOException {
        runLogin();
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/HalamanAwal.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnRegister(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/RegisterPenyediaKerja.fxml"));
        rootPane.getChildren().setAll(pane);
    }
    
}
