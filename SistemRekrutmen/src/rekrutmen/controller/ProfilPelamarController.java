/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import static rekrutmen.controller.DetailLowonganSimpanController.deskripsiPekerjaans;
import static rekrutmen.controller.DetailLowonganSimpanController.gajis;
import static rekrutmen.controller.DetailLowonganSimpanController.kualifikasiPekerjaans;
import static rekrutmen.controller.DetailLowonganSimpanController.namaPekerjaans;
import static rekrutmen.controller.DetailLowonganSimpanController.namaPerusahaans;
import static rekrutmen.controller.HomePenyediaKerjaController.idLamaran;
import static rekrutmen.controller.ListLamaranController.id;
import static rekrutmen.controller.ListLamaranController.statusLamarans;
import static rekrutmen.controller.ListLamaranController.textResumes;
import static rekrutmen.controller.ValidasiLamaranController.idLamaran;
import rekrutmen.model.Lamaran;
import rekrutmen.model.Perusahaan;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class ProfilPelamarController implements Initializable {

    private TextField resume;
    
    private static String QUERY_PELAMAR="from Pelamar";
    
    private static int idPelamar =0;
    
    public String namas, emails, pass, alamats, noTelps, resumes; 
    @FXML
    private TextField emailPelamar;
    @FXML
    private TextArea alamatPelamar;
    @FXML
    private TextField noTelp;
    @FXML
    private TextField namaPelamar;
    
    public String textResume;
    
    ObservableList<Lamaran> listLamaran = null;
    public static int id =0;
    @FXML
    private TextField passwordPelamar;
    @FXML
    private AnchorPane rootPane;
 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setData();
        loadData();
    }    

    public void setData(){
        this.idPelamar = LoginController.idPelamar;
        this.namas = LoginController.namas;
        this.emails = LoginController.emails; 
        this.alamats = LoginController.alamat;            
        this.noTelps = LoginController.noTelp; 
        this.pass = LoginController.pass; 
    }
    
    public void loadData(){
        namaPelamar.setText(namas);
        emailPelamar.setText(emails);
        alamatPelamar.setText(alamats);
        noTelp.setText(noTelps);
        passwordPelamar.setText(pass);
    }
    
    @FXML
    private void keluar(MouseEvent event) {
    }

    @FXML
    private void btnLamarLowongan(ActionEvent event) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        ses.beginTransaction();
        Query q = ses.createSQLQuery("UPDATE pelamar SET username='"+namaPelamar.getText()+"', password='"+passwordPelamar.getText()+"', email='"+emailPelamar.getText()+"', no_telp='"+noTelp.getText()+"', alamat='"+alamatPelamar.getText()+"' WHERE id_pelamar='"+idPelamar+"'");
        q.executeUpdate();
        ses.getTransaction().commit();
        ses.close();
        
        Alert alert = new Alert(Alert.AlertType.NONE, "Anda telah mengubah profil anda.", ButtonType.OK);
        alert.setTitle("Validasi Lamaran");
        alert.showAndWait();
    }

    private void btnResume(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File selectedFile =  chooser.showOpenDialog(null);
        
        if(selectedFile!=null){
            resume.setText(selectedFile.getPath());
        }else{
            Alert alert = new Alert(Alert.AlertType.NONE, "Silahkan memilih resume anda", ButtonType.OK);
            alert.setTitle("Berhasil");
            alert.showAndWait();
        }
    }

    private void btnResetResume(ActionEvent event) {
        resume.setText("");
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Home.fxml"));
            rootPane.getChildren().setAll(pane);
    }
    
}
