/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import static rekrutmen.controller.DetailLowonganController.deskripsiPekerjaans;
import static rekrutmen.controller.DetailLowonganController.gajis;
import static rekrutmen.controller.DetailLowonganController.kualifikasiPekerjaans;
import static rekrutmen.controller.DetailLowonganController.namaPekerjaans;
import static rekrutmen.controller.DetailLowonganController.namaPerusahaans;
import rekrutmen.model.Perusahaan;
import rekrutmen.model.Lowongan;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class FormTambahLowonganController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TextField namaPekerjaan;
    @FXML
    private TextArea deskripsiPekerjaan;
    @FXML
    private TextArea kualifikasiPekerjaan;
    @FXML
    private TextField gaji;
    
    ObservableList<Perusahaan> listPerusahaan = null;
    ObservableList<Lowongan> listLowongan = null;
    
    private static String QUERY_PERUSAHAAN="from Perusahaan";
    
    private static String QUERY_LOWONGAN="from Lowongan";
    
    @FXML
    private TextField namaPerusahaan;
    @FXML
    private TextField id_perusahaan;
    public static String namaPerusahaans, namaPekerjaans, deskripsiPekerjaans, kualifikasiPekerjaans, gajis;
    
    public int idPer;
    public String namaPer;
    public String id;
    
    public static int idLowongan =0;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tampil();
    }    
    
    private void executeQueryAmbilPerusahaan(String hql){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Perusahaan");
            List resultList = q.list();
            for(Object o : resultList){
                Perusahaan p = (Perusahaan)o;
                idPer = p.getIdPerusahaan();
                namaPer = p.getNamaPerusahaan();
            }
            session.getTransaction().commit();
//            session.getTransaction().commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    public void tampil(){
        executeQueryAmbilPerusahaan(QUERY_PERUSAHAAN);
    }

    private void executeHQLQueryInsert(){
        try{
            Session ses = HibernateUtil.getSessionFactory().openSession();
            ses.beginTransaction();
            Query q = ses.createSQLQuery("Insert into lowongan (nama_perusahaan, job_title, job_description, job_kualification, salary, id_perusahaan) values (:nama_perusahaan, :job_title, :job_description, :job_kualification, :salary, :id_perusahaan)");
            q.setParameter("nama_perusahaan", namaPer);
            q.setParameter("job_title", namaPekerjaan.getText());
            q.setParameter("job_description", deskripsiPekerjaan.getText());
            q.setParameter("job_kualification", kualifikasiPekerjaan.getText());
            q.setParameter("salary", gaji.getText());
//            id = Integer.toString(idPer);
//            id_perusahaan.setText(id);
            q.setParameter("id_perusahaan", idPer);
            q.executeUpdate();
            ses.getTransaction().commit();
            ses.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    @FXML
    private void btnTambahLowongan(ActionEvent event) throws IOException {
        if (namaPekerjaan.getText().equals("") || deskripsiPekerjaan.getText().equals("") || kualifikasiPekerjaan.getText().equals("") || gaji.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
            executeHQLQueryInsert();
            Alert alert = new Alert(Alert.AlertType.NONE, "Lowongan baru Telah ditambahkan", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
             AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/HomePenyediaKerja.fxml"));
            rootPane.getChildren().setAll(pane);
        }
    }

    @FXML
    private void keluar(MouseEvent event) {
    }

    @FXML
    private void btnReset(ActionEvent event) {
        namaPerusahaan.setText("");
        namaPekerjaan.setText("");
        deskripsiPekerjaan.setText("");
        kualifikasiPekerjaan.setText("");
        gaji.setText("");
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/HomePenyediaKerja.fxml"));
        rootPane.getChildren().setAll(pane);
    }
}

