/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import config.Hibernate.HibernateUtil;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import static rekrutmen.controller.DetailLowonganController.deskripsiPekerjaans;
import static rekrutmen.controller.DetailLowonganController.gajis;
import static rekrutmen.controller.DetailLowonganController.kualifikasiPekerjaans;
import static rekrutmen.controller.DetailLowonganController.namaPekerjaans;
import static rekrutmen.controller.DetailLowonganController.namaPerusahaans;
import rekrutmen.model.Lamaran;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class ValidasiLamaranController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TextField namaPekerjaan;
    private TextArea deskripsiPekerjaan;
    @FXML
    private TextArea kualifikasiPekerjaan;
    @FXML
    private TextField namaPelamar;
    @FXML
    private TextField resumePelamar;
    
    public static String namaPelamars, resumes, namaPekerjaans, kualifikasiPekerjaans, statusLamarans;
    
    public static int idLamaran= 0;
    @FXML
    private TextField statusLamaran;
 
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setData();
        loadData();
    }    
    
    

    @FXML
    private void keluar(MouseEvent event) {
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/HomePenyediaKerja.fxml"));
            rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnTolakLamaran(ActionEvent event) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        ses.beginTransaction();
        Query q = ses.createSQLQuery("UPDATE lamaran SET status_lamaran='Telah Ditolak' WHERE id_lamaran='"+idLamaran+"'");
        q.executeUpdate();
        ses.getTransaction().commit();
        ses.close();
        
        Alert alert = new Alert(Alert.AlertType.NONE, "Anda Telah Menolak Lamaran.", ButtonType.OK);
        alert.setTitle("Validasi Lamaran");
        alert.showAndWait();
    }

    @FXML
    private void btnTerimaLamaran(ActionEvent event) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        ses.beginTransaction();
        Query q = ses.createSQLQuery("UPDATE lamaran SET status_lamaran='Telah Diterima' WHERE id_lamaran='"+idLamaran+"'");
        q.executeUpdate();
        ses.getTransaction().commit();
        ses.close();
        
        Alert alert = new Alert(Alert.AlertType.NONE, "Anda Telah Menerima Lamaran.", ButtonType.OK);
        alert.setTitle("Validasi Lamaran");
        alert.showAndWait();
    }
    
    public void setData(){
        this.idLamaran = HomePenyediaKerjaController.idLamaran;
        this.namaPekerjaans = HomePenyediaKerjaController.namaPekerjaans;        
        this.kualifikasiPekerjaans = HomePenyediaKerjaController.kualifikasiPekerjaans;
        this.namaPelamars = HomePenyediaKerjaController.namaPelamars;
        this.resumes = HomePenyediaKerjaController.resumes;
        this.statusLamarans = HomePenyediaKerjaController.statusLamarans;
    }
    
    public void loadData(){
        namaPekerjaan.setText(namaPekerjaans);
        kualifikasiPekerjaan.setText(kualifikasiPekerjaans);
        namaPelamar.setText(namaPelamars);
        resumePelamar.setText(resumes);
        statusLamaran.setText(statusLamarans);
    }
    
}
