/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import static rekrutmen.controller.HomePenyediaKerjaController.id;
import static rekrutmen.controller.HomePenyediaKerjaController.idLamaran;
import rekrutmen.model.Lamaran;
import rekrutmen.model.Perusahaan;
import config.Hibernate.HibernateUtil;
import java.io.IOException;
import javafx.fxml.FXMLLoader;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class ListLamaranController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TableView<Lamaran> tablePelamaran;
    @FXML
    private TableColumn rNama_pekerjaan;
    @FXML
    private TableColumn rKualifikasiPekerjaan;
    @FXML
    private TableColumn rResume;
    @FXML
    private TableColumn rStatusLamaran;

    ObservableList<Lamaran> listLamaran = null;
    
    ObservableList<Perusahaan> listPerusahaan = null;
    
    public static String namaPelamars, resumes, namaPerusahaans, namaPekerjaans, kualifikasiPekerjaans, statusLamarans;
    public static int id =0;
    private static String QUERY_LAMARAN="FROM Lamaran E WHERE E.idPelamar = :id_pelamar" ;
    
    private static String QUERY_PERUSAHAAN="from Perusahaan" ;
    
    private String status;
    
    public static String textResumes;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tampil();
    }    
    
    private void executeHQLQuery(String hql){
        try{
            Session session = config.Hibernate.HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("FROM Lamaran E WHERE E.idPelamar = :id_pelamar");
            this.id = LoginController.idPelamar;
            q.setParameter("id_pelamar", id);
            List resultList = q.list();
            displayResult(resultList);
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    private void displayResult(List resultList){
        listLamaran = FXCollections.observableArrayList();
//        PemesananModel pm = new PemesananModel();
        
        for(Object m : resultList){
            Lamaran lamaran = (Lamaran)m;
            listLamaran.add(lamaran);
            idLamaran = lamaran.getIdLamaran();
            statusLamarans = lamaran.getStatusLamaran();
            textResumes = lamaran.getResume();
        }
        
        rNama_pekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobTitle"));
        rKualifikasiPekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobKualification"));
        rResume.setCellValueFactory(new PropertyValueFactory<>("resume"));
        rStatusLamaran.setCellValueFactory(new PropertyValueFactory<>("statusLamaran"));
        
        tablePelamaran.getColumns().clear();
        tablePelamaran.setItems(listLamaran);
        tablePelamaran.getColumns().addAll(rNama_pekerjaan, rKualifikasiPekerjaan, rResume, rStatusLamaran);
        
        tablePelamaran.setOnMouseClicked(new EventHandler<MouseEvent>(){
           @Override
           public void handle(MouseEvent event){
               pilihText();
           }
        });
    }
    
    public void pilihText(){
        
        if(tablePelamaran.getSelectionModel().getSelectedItem() != null){
            Lamaran selectedTable = tablePelamaran.getSelectionModel().getSelectedItem();
//            this.ids = selectedTable.getIdLowongan();
//            dId = Integer.toString(this.ids);
//            id.setText(dId);
            this.namaPekerjaans = selectedTable.getJobTitle(); 
            this.resumes = selectedTable.getResume();      
            textResumes = this.resumes;
            this.kualifikasiPekerjaans = selectedTable.getJobKualification();
            this.statusLamarans = selectedTable.getStatusLamaran();
            
        }
    }
   
    public void tampil(){
        executeHQLQuery(QUERY_LAMARAN);
    }

    @FXML
    private void keluar(MouseEvent event) {
    }

    private void executeHQLQueryDelete(){
        try{
            Session ses = HibernateUtil.getSessionFactory().openSession();
            ses.beginTransaction();
            Query q = ses.createSQLQuery("Delete from lamaran WHERE id_lamaran='"+idLamaran+"'");
            q.executeUpdate();
            ses.getTransaction().commit();
            ses.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    @FXML
    private void btnLihatDetail(ActionEvent event) {
        if(status == "Telah Diterima"){
            Alert alert = new Alert(Alert.AlertType.NONE, "Anda Tidak bisa menghapus status yang telah diterima", ButtonType.OK);
            alert.setTitle("Tidak menghapus lamaran");
            alert.showAndWait();
        }else{
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Produk Telah dihapus", ButtonType.OK);
            alert.setTitle("Berhasil");
            alert.showAndWait();
        }
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Home.fxml"));
            rootPane.getChildren().setAll(pane);
    }
    
}
