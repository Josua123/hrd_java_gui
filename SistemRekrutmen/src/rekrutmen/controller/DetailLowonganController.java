/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import rekrutmen.model.Pelamar;
import rekrutmen.model.Perusahaan;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class DetailLowonganController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TextField namaPekerjaan;
    @FXML
    private TextArea deskripsiPekerjaan;
    @FXML
    private TextArea kualifikasiPekerjaan;
    @FXML
    private TextField gaji;
    
    ObservableList<Pelamar> listPelamar = null;
    
    ObservableList<Perusahaan> listPerusahaan = null;
    
    private static String QUERY_PERUSAHAAN="from Perusahaan";
    
    private static String QUERY_PELAMAR="from Pelamar";

    public static int id =0;
    public static String namaPerusahaans, namaPekerjaans, deskripsiPekerjaans, kualifikasiPekerjaans, gajis;
    public static int dId;
    @FXML
    private TextField namaPerusahaan;
    @FXML
    private TextField resume;
    
    public int idPer;
    public int idPel;
    public String namaPel;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setData();
        loadData();
        tampilPerusahaan();
        tampilPelamar();
    }    
    
    private void executeQueryAmbilPerusahaan(String hql){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Perusahaan");
            List resultList = q.list();
            for(Object o : resultList){
                Perusahaan ph= (Perusahaan)o;
                idPer = ph.getIdPerusahaan();
            }
            session.getTransaction().commit();
//            session.getTransaction().commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    public void tampilPerusahaan(){
        executeQueryAmbilPerusahaan(QUERY_PERUSAHAAN);
    }
    
    private void executeQueryAmbilPelamar(String hql){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Pelamar");
            List resultList = q.list();
            for(Object o : resultList){
                Pelamar pl = (Pelamar)o;
                idPel = pl.getIdPelamar();
                namaPel = pl.getEmail();
            }
            session.getTransaction().commit();
//            session.getTransaction().commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    public void tampilPelamar(){
        executeQueryAmbilPelamar(QUERY_PELAMAR);
    }
    
    public void setData(){
        this.id = HomeController.id;
        this.namaPerusahaans = HomeController.namaPerusahaans;
        this.namaPekerjaans = HomeController.namaPekerjaans; 
        this.deskripsiPekerjaans = HomeController.deskripsiPekerjaans;            
        this.kualifikasiPekerjaans = HomeController.kualifikasiPekerjaans;
        this.gajis = HomeController.gajis;  
    }
    
    public void loadData(){
        namaPerusahaan.setText(namaPerusahaans);
        namaPekerjaan.setText(namaPekerjaans);
        deskripsiPekerjaan.setText(deskripsiPekerjaans);
        kualifikasiPekerjaan.setText(kualifikasiPekerjaans);
        gaji.setText(gajis);
    }

    @FXML
    private void btnLamarLowongan(ActionEvent event) throws IOException {
        if(namaPerusahaan.getText().equals("") || namaPekerjaan.getText().equals("") || deskripsiPekerjaan.getText().equals("") || kualifikasiPekerjaan.getText().equals("") || gaji.getText().equals("") || resume.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.NONE, "Silahkan mengisi seluruh data pada form", ButtonType.OK);
            alert.setTitle("Kesalahan pada pendaftaran");
            alert.showAndWait();
        }else{
            Session ses = HibernateUtil.getSessionFactory().openSession();
            ses.beginTransaction();
            Query q = ses.createSQLQuery("Insert into lamaran (nama_perusahaan, job_title, job_description, job_kualification, salary, resume, nama_pelamar, status_lamaran, id_pelamar, id_perusahaan) values (:nama_perusahaan, :job_title, :job_description, :job_kualification, :salary, :resume, :nama_pelamar, :status_lamaran, :id_pelamar, :id_perusahaan)");
            q.setParameter("nama_perusahaan", namaPerusahaan.getText());
            q.setParameter("job_title", namaPekerjaan.getText());
            q.setParameter("job_description", deskripsiPekerjaan.getText());
            q.setParameter("job_kualification", kualifikasiPekerjaan.getText());
            q.setParameter("salary", gaji.getText());
            q.setParameter("resume", resume.getText());
            q.setParameter("nama_pelamar", namaPel);
            q.setParameter("id_pelamar", idPel);
            q.setParameter("id_perusahaan", idPer);
            q.setParameter("status_lamaran", "menunggu");
            q.executeUpdate();
            ses.getTransaction().commit();
            ses.close();
            Alert alert = new Alert(Alert.AlertType.NONE, "Lamaran anda telah dikirimkan. Terima kasih telah melamar.", ButtonType.OK);
            alert.setTitle("Berhasil Melamar");
            alert.showAndWait();
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Home.fxml"));
            rootPane.getChildren().setAll(pane);
        }
    }

    @FXML
    private void btnResume(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File selectedFile =  chooser.showOpenDialog(null);
        
        if(selectedFile!=null){
            resume.setText(selectedFile.getPath());
        }else{
            Alert alert = new Alert(Alert.AlertType.NONE, "Silahkan memilih resume anda", ButtonType.OK);
            alert.setTitle("Berhasil");
            alert.showAndWait();
        }
    }

    @FXML
    private void btnResetResume(ActionEvent event) {
        resume.setText("");
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Home.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void keluar(MouseEvent event) {
        
    }
    
}
