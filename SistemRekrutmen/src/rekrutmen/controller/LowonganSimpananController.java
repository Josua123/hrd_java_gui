/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import static rekrutmen.controller.HomeController.id;
import static rekrutmen.controller.HomeController.idPelamar;
import rekrutmen.model.Lowongan;
import rekrutmen.model.Lowongantersimpan;
import config.Hibernate.HibernateUtil;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class LowonganSimpananController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TableView<Lowongantersimpan> tableLowongan;
    @FXML
    private TableColumn rNama_perusahaan;
    @FXML
    private TableColumn rNama_pekerjaan;
    @FXML
    private TableColumn rDeskripsi_pekerjaan;
    @FXML
    private TableColumn rKualifikasi_pekerjaan;
    @FXML
    private TableColumn rGaji;
    
    ObservableList<Lowongantersimpan> listLowongan = null;
    
    private static String QUERY_LOWONGANTERSIMPAN="FROM Lowongantersimpan E WHERE E.idPelamar = :id_pelamar";
    
    public static int idLowongan =0;
    public static String namaPerusahaans, namaPekerjaans, deskripsiPekerjaans, kualifikasiPekerjaans, gajis;

    public static int idPelamar=0;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tampil();
    }    
    
    private void executeHQLQuery(String hql){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("FROM Lowongantersimpan E WHERE E.idPelamar = :id_pelamar");
            this.idPelamar = LoginController.idPelamar;
            q.setParameter("id_pelamar", idPelamar);
            List resultList = q.list();
            displayResult(resultList);
//            session.getTransaction().commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    private void displayResult(List resultList){
        listLowongan = FXCollections.observableArrayList();
//        PemesananModel pm = new PemesananModel();
        
        for(Object m : resultList){
            Lowongantersimpan lowongantersimpan = (Lowongantersimpan)m;
            listLowongan.add(lowongantersimpan);
            idLowongan = lowongantersimpan.getIdLamarantersimpan();
        }
        
        rNama_perusahaan.setCellValueFactory(new PropertyValueFactory<>("namaPerusahaan"));
        rNama_pekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobTitle"));
        rDeskripsi_pekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobDescription"));
        rKualifikasi_pekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobKualification"));
        rGaji.setCellValueFactory(new PropertyValueFactory<>("salary"));
        
        tableLowongan.getColumns().clear();
        tableLowongan.setItems(listLowongan);
        tableLowongan.getColumns().addAll(rNama_perusahaan, rNama_pekerjaan, rDeskripsi_pekerjaan, rKualifikasi_pekerjaan, rGaji);
        
        tableLowongan.setOnMouseClicked(new EventHandler<MouseEvent>(){
           @Override
           public void handle(MouseEvent event){
               pilihText();
           }
        });
    }
    
    public void pilihText(){
        
        if(tableLowongan.getSelectionModel().getSelectedItem() != null){
            Lowongantersimpan selectedTable = tableLowongan.getSelectionModel().getSelectedItem();
            this.namaPerusahaans = selectedTable.getNamaPerusahaan();
            this.namaPekerjaans = selectedTable.getJobTitle(); 
            this.deskripsiPekerjaans = selectedTable.getJobDescription();            
            this.kualifikasiPekerjaans = selectedTable.getJobKualification();
            this.gajis = selectedTable.getSalary();  
        }
    }
    
    public void tampil(){
        executeHQLQuery(QUERY_LOWONGANTERSIMPAN);
    }
    @FXML
    private void keluar(MouseEvent event) {
    }

    @FXML
    private void btnDetailLowongan(ActionEvent event) throws IOException {
        if(idLowongan == 0){
            Alert eAlert = new Alert(Alert.AlertType.WARNING);
            eAlert.setHeaderText(null);
            eAlert.setContentText("Tidak ada data yang dipilih");
            eAlert.showAndWait();
        }else{
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/DetailLowonganSimpan.fxml"));
            rootPane.getChildren().setAll(pane); 
        }
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Home.fxml"));
            rootPane.getChildren().setAll(pane); 
    }
    
}
