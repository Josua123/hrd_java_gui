/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.hibernate.Query;
import org.hibernate.Session;
import rekrutmen.model.Pelamar;

/**
 * FXML Controller class
 *
 * @author Josua_Marpaung
 */
public class LoginController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TextField password;
    
    public String savedEmail;
    public String savedPassword;
    private static String QUERY_LOGIN = "from Pelamar a where a.email like '";
    @FXML
    private TextField email;
    
    public static int idPelamar=0;
    
    public static String namas, pass, emails, alamat, noTelp, resumes; 
    
       /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    private void runLogin(){
        executeHQLQuery(QUERY_LOGIN + email.getText() + "'");
    }
    
    private void executeHQLQuery(String sql){
        try{
            Session ses = HibernateUtil.getSessionFactory().openSession();
            ses.beginTransaction();
            Query q = ses.createQuery(sql);
            List resultList = q.list();
            for(Object o : resultList){
                Pelamar p = (Pelamar)o;
                savedEmail = p.getEmail();
                savedPassword = p.getPassword();
                idPelamar = p.getIdPelamar();
                alamat = p.getAlamat();
                noTelp = p.getNoTelp();
                namas = p.getUsername();
                emails = this.savedEmail;
                pass = this.savedPassword;
                alamat = this.alamat;
                noTelp = this.noTelp;
            }
            ses.getTransaction().commit();
            if(savedEmail == null || savedPassword == null){
                autentikasi("", "");
            }else{
                autentikasi(savedEmail, savedPassword);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void autentikasi(String savedUsername, String savedPassword) throws IOException {
        if(savedUsername.equals(email.getText()) && savedPassword.equals(password.getText())){
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Home.fxml"));
            rootPane.getChildren().setAll(pane);
        }else{
            Alert alert = new Alert(Alert.AlertType.NONE, "Silahkan mengisi atau mengecek seluruh data yang ada pada form", ButtonType.OK);
            alert.setTitle("Kesalahan pada Login");
            alert.showAndWait();
        }
    }
    
    @FXML
    private void btnLogin(ActionEvent event) {
        runLogin();
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/HalamanAwal.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnRegister(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Register.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnSkip(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/SkipHome.fxml"));
        rootPane.getChildren().setAll(pane);
    }
}
