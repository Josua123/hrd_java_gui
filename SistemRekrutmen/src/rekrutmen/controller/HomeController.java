/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import static rekrutmen.controller.DetailLowonganController.deskripsiPekerjaans;
import static rekrutmen.controller.DetailLowonganController.gajis;
import static rekrutmen.controller.DetailLowonganController.kualifikasiPekerjaans;
import static rekrutmen.controller.DetailLowonganController.namaPekerjaans;
import static rekrutmen.controller.DetailLowonganController.namaPerusahaans;
import rekrutmen.model.Lowongan;
import rekrutmen.model.Perusahaan;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class HomeController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TableView<Lowongan> tableLowongan;
    @FXML
    private TableColumn rNama_perusahaan;
    @FXML
    private TableColumn rNama_pekerjaan;
    @FXML
    private TableColumn rDeskripsi_pekerjaan;
    @FXML
    private TableColumn rKualifikasi_pekerjaan;
    @FXML
    private TableColumn rGaji;
    ObservableList<Lowongan> listLowongan = null;
    
    private static String QUERY_LOWONGAN="from Lowongan";
    
    ObservableList<Perusahaan> listPerusahaan = null;
    
    private static String QUERY_PERUSAHAAN="from Perusahaan";
    
    private static String QUERY_BASED_CARI_JENIS_BARANG="from Lowongan a where a.jobTitle like '";
    
    public static int id =0;
    public static String namaPerusahaans, namaPekerjaans, deskripsiPekerjaans, kualifikasiPekerjaans, gajis;
    public static int dId;
    public static int idPer;
    public static int idPelamar=0;
    @FXML
    private TextField cariText;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tampil();
        tampilPerusahaan();
    }    
    
    private void runQueryBasedOnFirstName(){
        executeQueryCari(QUERY_BASED_CARI_JENIS_BARANG + cariText.getText() + "%'");
    }

    private void executeQueryCari(String hql){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(hql);
            List resultList = q.list();
            displayResult(resultList);
            session.getTransaction().commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    private void executeQueryAmbilPerusahaan(String hql){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Perusahaan");
            List resultList = q.list();
            for(Object o : resultList){
                Perusahaan ph= (Perusahaan)o;
                idPer = ph.getIdPerusahaan();
            }
            session.getTransaction().commit();
//            session.getTransaction().commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    public void tampilPerusahaan(){
        executeQueryAmbilPerusahaan(QUERY_PERUSAHAAN);
    }

    private void executeHQLQuery(String hql){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Lowongan");
            List resultList = q.list();
            displayResult(resultList);
//            session.getTransaction().commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
    }
    
    private void displayResult(List resultList){
        listLowongan = FXCollections.observableArrayList();
//        PemesananModel pm = new PemesananModel();
        
        for(Object m : resultList){
            Lowongan lowongan = (Lowongan)m;
            listLowongan.add(lowongan);
            idPelamar = LoginController.idPelamar;
        }
        
        rNama_perusahaan.setCellValueFactory(new PropertyValueFactory<>("namaPerusahaan"));
        rNama_pekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobTitle"));
        rDeskripsi_pekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobDescription"));
        rKualifikasi_pekerjaan.setCellValueFactory(new PropertyValueFactory<>("jobKualification"));
        rGaji.setCellValueFactory(new PropertyValueFactory<>("salary"));
        
        tableLowongan.getColumns().clear();
        tableLowongan.setItems(listLowongan);
        tableLowongan.getColumns().addAll(rNama_perusahaan, rNama_pekerjaan, rDeskripsi_pekerjaan, rKualifikasi_pekerjaan, rGaji);
        
        tableLowongan.setOnMouseClicked(new EventHandler<MouseEvent>(){
           @Override
           public void handle(MouseEvent event){
               pilihText();
           }
        });
    }
    
    public void pilihText(){
        
        if(tableLowongan.getSelectionModel().getSelectedItem() != null){
            Lowongan selectedTable = tableLowongan.getSelectionModel().getSelectedItem();
            id = selectedTable.getIdLowongan();
            this.namaPerusahaans = selectedTable.getNamaPerusahaan();
            this.namaPekerjaans = selectedTable.getJobTitle(); 
            this.deskripsiPekerjaans = selectedTable.getJobDescription();            
            this.kualifikasiPekerjaans = selectedTable.getJobKualification();
            this.gajis = selectedTable.getSalary();  
        }
    }
    
    public void tampil(){
        executeHQLQuery(QUERY_LOWONGAN);
    }
    
//    public void setText(){
//        this.deskripsi = deskripsis;
//    }
    
    @FXML
    private void keluar(MouseEvent event) {
    }

    @FXML
    private void btnPekerjaanSaved(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/LowonganSimpanan.fxml"));
        rootPane.getChildren().setAll(pane); 
    }

    @FXML
    private void btnHelp(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Help.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnAboutUs(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/About Us.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnSimpan(ActionEvent event) {
        if(id == 0){
            Alert eAlert = new Alert(Alert.AlertType.WARNING);
            eAlert.setHeaderText(null);
            eAlert.setContentText("Tidak ada data yang dipilih");
            eAlert.showAndWait();
        }else{
            Session ses = HibernateUtil.getSessionFactory().openSession();
            ses.beginTransaction();
            Query q = ses.createSQLQuery("Insert into lowongantersimpan (nama_perusahaan, job_title, job_description, job_kualification, salary, id_pelamar, id_perusahaan) values (:nama_perusahaan, :job_title, :job_description, :job_kualification, :salary, :id_pelamar, :id_perusahaan)");
            q.setParameter("nama_perusahaan", namaPerusahaans);
            q.setParameter("job_title", namaPekerjaans);
            q.setParameter("job_description", deskripsiPekerjaans);
            q.setParameter("job_kualification", kualifikasiPekerjaans);
            q.setParameter("salary", gajis);
            q.setParameter("id_pelamar", idPelamar);
            q.setParameter("id_perusahaan", idPer);
            q.executeUpdate();
            ses.getTransaction().commit();
            ses.close();
            Alert alert = new Alert(Alert.AlertType.NONE, "Anda telah membookmark sebuah lamaran, silahkan melihat pada lamaran tersimpan,", ButtonType.OK);
            alert.setTitle("Berhasil Melamar");
            alert.showAndWait();
        }
    }

    @FXML
    private void btnApply(ActionEvent event) throws IOException {
        if(id == 0){
            Alert eAlert = new Alert(Alert.AlertType.WARNING);
            eAlert.setHeaderText(null);
            eAlert.setContentText("Tidak ada data yang dipilih");
            eAlert.showAndWait();
        }else{
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/DetailLowongan.fxml"));
            rootPane.getChildren().setAll(pane); 
        }
    }

    @FXML
    private void btnPekerjaanYangDilamar(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/ListLamaran.fxml"));
            rootPane.getChildren().setAll(pane); 
    }

    @FXML
    private void btnLogout(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/Login.fxml"));
            rootPane.getChildren().setAll(pane); 
    }

    @FXML
    private void cariLowongan(ActionEvent event) {
        if(!cariText.getText().trim().equals("")){
            runQueryBasedOnFirstName();
        }
    }

    @FXML
    private void btnProfil(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/ProfilPelamar.fxml"));
            rootPane.getChildren().setAll(pane); 
    }
    
}
