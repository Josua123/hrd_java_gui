/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import static rekrutmen.controller.FormTambahLowonganController.deskripsiPekerjaans;
import static rekrutmen.controller.FormTambahLowonganController.gajis;
import static rekrutmen.controller.FormTambahLowonganController.idLowongan;
import static rekrutmen.controller.FormTambahLowonganController.kualifikasiPekerjaans;
import static rekrutmen.controller.FormTambahLowonganController.namaPekerjaans;
import static rekrutmen.controller.FormTambahLowonganController.namaPerusahaans;
import rekrutmen.model.Lowongan;
import rekrutmen.model.Perusahaan;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class MengelolaLowonganController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TextField namaPekerjaan;
    @FXML
    private TextArea deskripsiPekerjaan;
    @FXML
    private TextArea kualifikasiPekerjaan;
    @FXML
    private TextField gaji;
 
    public static String namaPerusahaans, namaPekerjaans, deskripsiPekerjaans, kualifikasiPekerjaans, gajis;
  
    public static int id =0;
    public static int idLowongan =0;
    @FXML
    private TextField namaPerusahaan;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setData();
        loadData();
    }    

    @FXML
    private void keluar(MouseEvent event) {
    }

    private void executeHQLQueryUpdate(){
        try{
            Session ses = HibernateUtil.getSessionFactory().openSession();
            ses.beginTransaction();
            Query q = ses.createSQLQuery("UPDATE Lowongan SET nama_perusahaan = '"+namaPerusahaan.getText()+"', job_title = '"+namaPekerjaan.getText()+"' , job_description ='"+deskripsiPekerjaan.getText()+"', job_kualification ='"+kualifikasiPekerjaan.getText()+"', salary = '"+gaji.getText()+"', id_perusahaan = '"+id+"' Where id_lowongan='"+idLowongan+"'");
            q.executeUpdate();
            ses.getTransaction().commit();
            ses.close();     
        }catch(Exception e){
            e.printStackTrace();
        }
    }
  
    public void setData(){
        this.id = LowonganKamiController.id;
        this.idLowongan = LowonganKamiController.idLowongan;
        this.namaPerusahaans = LowonganKamiController.namaPerusahaans;
        this.namaPekerjaans = LowonganKamiController.namaPekerjaans; 
        this.deskripsiPekerjaans = LowonganKamiController.deskripsiPekerjaans;            
        this.kualifikasiPekerjaans = LowonganKamiController.kualifikasiPekerjaans;
        this.gajis = LowonganKamiController.gajis;  
    }
    
    public void loadData(){
        namaPerusahaan.setText(namaPerusahaans);
        namaPekerjaan.setText(namaPekerjaans);
        deskripsiPekerjaan.setText(deskripsiPekerjaans);
        kualifikasiPekerjaan.setText(kualifikasiPekerjaans);
        gaji.setText(gajis);
    }
    
    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/LowonganKami.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    @FXML
    private void btnReset(ActionEvent event) {
        namaPerusahaan.setText("");
        namaPekerjaan.setText("");
        deskripsiPekerjaan.setText("");
        kualifikasiPekerjaan.setText("");
        gaji.setText("");
    }

    @FXML
    private void btnUpdateLowongan(ActionEvent event) {
        executeHQLQueryUpdate();
        Alert alert = new Alert(Alert.AlertType.NONE, "Data Barang Telah di perbaharui", ButtonType.OK);
        alert.setTitle("Berhasil");
        alert.showAndWait();
    }
    
}
