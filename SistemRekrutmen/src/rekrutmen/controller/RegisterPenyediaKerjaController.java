/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import config.Hibernate.HibernateUtil;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class RegisterPenyediaKerjaController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private PasswordField password;
    @FXML
    private TextField nama_lengkap;
    @FXML
    private TextField email;
    @FXML
    private TextField alamat;
    @FXML
    private TextField notelp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    

    @FXML
    private void btnRegister(ActionEvent event) throws IOException {
        if(nama_lengkap.getText().equals("") || password.getText().equals("") || email.getText().equals("") || alamat.getText().equals("") || notelp.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.NONE, "Silahkan mengisi seluruh data pada form", ButtonType.OK);
            alert.setTitle("Kesalahan pada pendaftaran");
            alert.showAndWait();
        }else{
            Session ses = HibernateUtil.getSessionFactory().openSession();
            ses.beginTransaction();
            Query q = ses.createSQLQuery("Insert into perusahaan (nama_perusahaan, email_perusahaan, password, alamat, no_telp) values (:nama_perusahaan, :email_perusahaan, :password, :alamat, :no_telp)");
            q.setParameter("nama_perusahaan", nama_lengkap.getText());
            q.setParameter("password", password.getText());
            q.setParameter("email_perusahaan", email.getText());
            q.setParameter("alamat", alamat.getText());
            q.setParameter("no_telp", notelp.getText());
            q.executeUpdate();
            ses.getTransaction().commit();
            ses.close();
            Alert alert = new Alert(Alert.AlertType.NONE, "Perusahaan anda Telah Terdaftar, silahkan login", ButtonType.OK);
            alert.setTitle("Berhasil Mendaftar");
            alert.showAndWait();
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/LoginPenyediaKerja.fxml"));
            rootPane.getChildren().setAll(pane);
        }
    }
    
    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/LoginPenyediaKerja.fxml"));
        rootPane.getChildren().setAll(pane);
    }
    
}
