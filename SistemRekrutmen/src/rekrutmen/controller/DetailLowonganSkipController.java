/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekrutmen.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import static rekrutmen.controller.DetailLowonganController.deskripsiPekerjaans;
import static rekrutmen.controller.DetailLowonganController.gajis;
import static rekrutmen.controller.DetailLowonganController.kualifikasiPekerjaans;
import static rekrutmen.controller.DetailLowonganController.namaPekerjaans;
import static rekrutmen.controller.DetailLowonganController.namaPerusahaans;

/**
 * FXML Controller class
 *
 * @author Bernika Siahaan;
 */
public class DetailLowonganSkipController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TextField namaPekerjaan;
    @FXML
    private TextArea deskripsiPekerjaan;
    @FXML
    private TextArea kualifikasiPekerjaan;
    @FXML
    private TextField gaji;
    @FXML
    private TextField namaPerusahaan;

    public static int id =0;
    public static String namaPerusahaans, namaPekerjaans, deskripsiPekerjaans, kualifikasiPekerjaans, gajis;
    public static int dId;
    public static int idLowongan = 0;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setData();
        loadData();
    }    

    public void setData(){
        this.idLowongan = SkipHomeController.idLowongan;
        this.namaPerusahaans = SkipHomeController.namaPerusahaans;
        this.namaPekerjaans = SkipHomeController.namaPekerjaans; 
        this.deskripsiPekerjaans = SkipHomeController.deskripsiPekerjaans;            
        this.kualifikasiPekerjaans = SkipHomeController.kualifikasiPekerjaans;
        this.gajis = SkipHomeController.gajis;  
    }
    
    public void loadData(){
        namaPerusahaan.setText(namaPerusahaans);
        namaPekerjaan.setText(namaPekerjaans);
        deskripsiPekerjaan.setText(deskripsiPekerjaans);
        kualifikasiPekerjaan.setText(kualifikasiPekerjaans);
        gaji.setText(gajis);
    }
    
    @FXML
    private void keluar(MouseEvent event) {
    }

    @FXML
    private void btnKembali(ActionEvent event) throws IOException {
         AnchorPane pane = FXMLLoader.load(getClass().getResource("/rekrutmen/ui/SkipHome.fxml"));
        rootPane.getChildren().setAll(pane);
    }
    
}
